var express = require('express');
const crypto = require('crypto');
const Avatar = require('avatar-builder');

// Create a hash, it will change each hour.
function getHash(data) {
	var date = new Date();
	var current_hour = date.getHours();
	data = data + current_hour.toString();
  // Repeat to create some load.
	for (i = 0; i < 1000; i++) {
	  data = data + crypto.createHash("sha512").update(data, "binary").digest("hex")
	}
  return crypto.createHash("sha512").update(data + current_hour.toString(), "binary").digest("hex");
}

var app = express();

app.get('/', function (req, res) {
	console.log("Request from: " + req.ip)
	res.send("Use http://<hostname>/getavatar/<id>");
});

app.get('/getavatar/:id', function (req, res) {
  console.log("Request hash from: " + req.ip)
  var hash=getHash(req.params.id);
  const avatar = Avatar.catBuilder(128);
  var ab = avatar.create(hash).then(buffer => {
    const buf6 = Buffer.from(buffer);
    res.write('<html><head></head><body>');
    res.write('<img src="data:image/png;base64,' + buf6.toString('base64') + ' " alt="Avatar">')
    res.end('</body></html>');
  });
});

app.listen(3000, "0.0.0.0", function () {
  console.log('Avatar building listening for requests on port 3000!');
});

