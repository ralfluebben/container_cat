# Install on AWS Ubuntu 18.04 LTS image

```
sudo apt update
sudo apt install nodejs npm
npm install --save express
npm install --save avatar-builder
npm install --save crypto
```

## Configure Security Group for the Node App

Add TCP port number 3000 to inbound rules of the related security group.

## Start the app
```
node app.js
```

# Usage

The provides a QRCode and a related hash for an id, to get it open the website

http://*hostname*/getavatar/*id*

Replace *hostname* with the public IP or hostname of your webserver and replace
*id* with any positive integer.




