FROM ubuntu:20.04
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y nodejs npm
RUN npm install --save express
RUN npm install --save avatar-builder
RUN npm install --save redis
COPY app.js /
CMD node /app.js

